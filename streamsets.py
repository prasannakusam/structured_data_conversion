import datetime


def date_today():
    return datetime.datetime.today()


def get_processing_date(delta_from_today=0):
    processing_date = date_today() - datetime.timedelta(delta_from_today)
    return processing_date.strftime('%Y/%m/%d')


def get_comparison_date(delta_from_today=1):
    comparison_date = date_today() - datetime.timedelta(delta_from_today)
    return comparison_date.strftime('%Y/%m/%d')


def main():
    print(get_processing_date())
    print(get_comparison_date())


if __name__ == '__main__':
    main()
