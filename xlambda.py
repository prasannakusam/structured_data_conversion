import xml.etree.cElementTree as ElementTree
from argparse import ArgumentParser
from lshared import (
    transform_name_to_uuid,
    fetch_filename_stem,
    get_dir_tree_size,
    bulk_delete_files,
    is_valid_format,
    gzip_extractor,
    get_sns_client,
    should_extract,
    get_s3_client,
    zip_extractor,
    get_timestamp,
    readable_size,
    readable_time
)
import tempfile
import datetime
import logging
import chardet
import shutil
import codecs
import ntpath
import time
import gzip
import json
import sys
import os
import re


source_article_split_params_map = {
    'pubmed': {
        'split_type': 'xml',
        'article_start_tag': 'PubmedArticle',
        'article_id_tag': 'PMID',
        'article_id_tag_items': [
            [('Version', '1')],
            [('Version', '2')],
            [('Version', '3')],
            [('Version', '4')]
        ]
    },
    'who': {
        'split_type': 'xml',
        'article_start_tag': 'doc',
        'article_id_tag': 'str',
        'article_id_tag_items': [[('name', 'id')]]
    },
    'cochrane': {
        'split_type': 'medline',
    }
}
post_decompress_move_map = {
    'pubmed': False,
    'cochrane': False,
    'who': False,
    'drugbank': True,
    'dailymed': True,
    'nct': True,
    'aps': False
}
source_split_map = {
    'pubmed': True,
    'cochrane': True,
    'who': True,
    'drugbank': False,
    'dailymed': False,
    'nct': False,
    'aps': False
}
source_fileformat_map = {
    'pubmed': 'xml',
    'cochrane': 'txt',
    'who': 'xml',
    'drugbank': 'json',
    'dailymed': 'xml',
    'nct': 'xml',
    'aps': 'html'
}
target_fileformat_map = {
    'pubmed': 'json',
    'cochrane': 'json',
    'who': 'json',
    'drugbank': 'json',
    'dailymed': 'json',
    'nct': 'json',
    'aps': 'txt'
}
transform_name_to_uuid_map = {
    'pubmed': False,
    'cochrane': False,
    'who': False,
    'drugbank': False,
    'dailymed': False,
    'nct': False,
    'aps': True
}
temp_dir = tempfile.mkdtemp(dir='/tmp/processing')


def get_logger(filename, log_file=None):
    logger = logging.getLogger(filename)
    logger.setLevel(logging.DEBUG)

    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    channel = logging.FileHandler(log_file) if log_file else \
        logging.StreamHandler(sys.stdout)
    channel.setLevel(logging.DEBUG)
    channel.setFormatter(formatter)

    logger.addHandler(channel)

    return logger


logger = get_logger(__file__)


def publish_to_sns(
        topic_arn,
        message_dict):
    sns_client = get_sns_client()
    response = sns_client.publish(
        TargetArn=topic_arn,
        Message=json.dumps({'default': json.dumps(message_dict)}),
        MessageStructure='json'
    )


def setup_s3_file_level_nomenclature(
        s3_in_file,
        source_name):
    current_timestamp = get_timestamp()
    s3_in_file_dir_key = os.path.dirname(s3_in_file)
    s3_in_file_dir_key_list = s3_in_file_dir_key.split(os.sep)
    s3_in_file_dir_key_source_name_index = s3_in_file_dir_key_list.index(
        source_name)
    s3_in_file_dir_key = os.sep.join(s3_in_file_dir_key_list[
        0: s3_in_file_dir_key_source_name_index + 1])

    s3_in_file_basename = ntpath.basename(s3_in_file)
    s3_in_filename_stem = fetch_filename_stem(s3_in_file_basename)

    s3_out_files_dir_key = s3_in_file_dir_key.replace('raw', 'processed')
    s3_out_files_decompressed_dir_key = os.path.join(
        s3_out_files_dir_key,
        'decompressed',
        # s3_in_filename_stem
    )

    source_fileformat = source_fileformat_map.get(source_name)
    local_in_file = os.path.join(
        temp_dir, ntpath.basename(s3_in_file_basename))
    local_out_decompressed_dir_all = os.path.join(
        temp_dir,
        'decompressed',
        'all_{}'.format(current_timestamp))
    local_out_decompressed_gzip_file_basename = '.'.join((
        s3_in_filename_stem,
        source_fileformat))
    local_out_decompressed_gzip_file = os.path.join(
        local_out_decompressed_dir_all,
        local_out_decompressed_gzip_file_basename)
    local_out_decompressed_zip_dir = os.path.join(
        temp_dir,
        s3_in_filename_stem)

    decompression_process_log_basename = '.'.join((
        'decompression_{}'.format(s3_in_filename_stem), 'log'))
    s3_in_files_process_log_dir_key = s3_in_file_dir_key.replace(
        'data', 'logs')
    s3_in_files_process_log_dir_key_list = \
        s3_in_files_process_log_dir_key.split(os.sep)
    s3_in_files_process_log_dir_key_list.remove('raw')
    s3_in_files_process_log_dir_key = os.sep.join(
        s3_in_files_process_log_dir_key_list)

    s3_in_file_decompression_process_log = os.path.join(
        s3_in_files_process_log_dir_key,
        decompression_process_log_basename)

    if not os.path.exists(local_out_decompressed_zip_dir):
        os.makedirs(local_out_decompressed_zip_dir)
    if not os.path.exists(local_out_decompressed_dir_all):
        os.makedirs(local_out_decompressed_dir_all)

    local_out_json_file_basename = '.'.join((s3_in_filename_stem, 'json'))
    local_out_json_file = os.path.join(temp_dir, local_out_json_file_basename)
    local_in_file_decompression_process_log = os.path.join(
        temp_dir, decompression_process_log_basename)

    return (s3_out_files_decompressed_dir_key,
            local_in_file,
            local_out_decompressed_gzip_file,
            local_out_decompressed_zip_dir,
            local_out_json_file,
            s3_in_file_decompression_process_log,
            local_in_file_decompression_process_log,
            local_out_decompressed_dir_all
            )


def extract_article_id(article_elem, article_id_tag, article_id_tag_items):
    for element in article_elem.findall(article_id_tag):
        if element.items() in article_id_tag_items:
            return element.text

    raise RunTimeError('Article ID not found')


def publish_article(
        s3_client,
        log_handle,
        s3_out_files_dir_key,
        local_article_file,
        source_name,
        bucket_name,
        sns_topic_base_name,
        converter_id,
        article_id,
        force_convert,
        remove_local_article=True):
    s3_article_file = os.path.join(
        s3_out_files_dir_key,
        ntpath.basename(local_article_file))
    s3_client.upload_file(
        local_article_file,
        bucket_name,
        s3_article_file)
    log_msg = 'article {} available @ {}'.format(
        article_id, s3_article_file)
    logger.debug(log_msg)
    log_msg = '{} - {}'.format(get_timestamp(2), log_msg)
    log_handle.write(log_msg)
    log_handle.write('\n')

    deltacon(
        source_name,
        bucket_name,
        s3_article_file,
        sns_topic_base_name,
        converter_id,
        force_convert)

    if remove_local_article:
        os.remove(local_article_file)


def deltacon(
        source_name,
        bucket_name,
        s3_article_file,
        sns_topic_base_name,
        converter_id,
        force_convert,
        strategy_name=None):
    source_fileformat = '.{}'.format(source_fileformat_map[source_name])
    target_fileformat = '.{}'.format(target_fileformat_map[source_name])

    s3_out_file = s3_article_file.replace(
        source_fileformat, target_fileformat).replace(
        'decompressed', 'deltacon')
    infile_basename = ntpath.basename(s3_article_file)
    outfile_basename = infile_basename.replace(
        source_fileformat, target_fileformat)
    sns_converter_topic_name = '{}-{}'.format(
        sns_topic_base_name, converter_id)
    sns_topic_arn = 'arn:aws:sns:us-east-2:'\
        '812531199550:{}'.format(sns_converter_topic_name)
    publish_to_sns(
        sns_topic_arn,
        dict(
            source_name=source_name,
            bucket_name=bucket_name,
            infile_basename=infile_basename,
            outfile_basename=outfile_basename,
            s3_in_file=s3_article_file,
            s3_out_file=s3_out_file,
            force_convert=force_convert,
            strategy_name=strategy_name))
    logger.debug('Published {} for conversion to'
                 ' SNS topic: {} using {}'.format(
                     s3_article_file,
                     sns_converter_topic_name,
                     strategy_name))


def file_split_and_upload(
        s3_client,
        bucket_name,
        source_name,
        s3_raw_file,
        local_out_file,
        local_out_split_dir,
        s3_out_files_dir_key,
        local_in_file_process_log,
        s3_in_file_process_log,
        sns_topic_base_name,
        converter_batch_size,
        force_convert=False):
    logger.debug('Starting Split process for {}...'.format(
        local_out_file))

    split_start = time.time()
    split_map = source_article_split_params_map[source_name]

    with open(local_in_file_process_log, 'a+') as lf:
        lf.write('{} - Starting Split process for {}...'.format(
            get_timestamp(2),
            s3_raw_file))
        lf.write('\n')

        converter_id = 1
        if split_map['split_type'] == 'xml':
            context = ElementTree.iterparse(local_out_file)
            for event, element in context:
                if element.tag == split_map['article_start_tag']:
                    if converter_id == converter_batch_size:
                        converter_id = 1

                    article_id = extract_article_id(
                        element, split_map['article_id_tag'],
                        split_map['article_id_tag_items'])
                    article_filename = '{}_article_{}.xml'.format(
                        source_name,
                        article_id)
                    local_article_file = os.path.join(local_out_split_dir,
                                                      article_filename)

                    with open(local_article_file, 'wb') as af:
                        af.write('<?xml version="1.0" '
                                 'encoding="UTF-8"?>\n'.encode(
                                     'utf8'))
                        af.write(ElementTree.tostring(element))

                    publish_article(
                        s3_client,
                        lf,
                        s3_out_files_dir_key,
                        local_article_file,
                        source_name,
                        bucket_name,
                        sns_topic_base_name,
                        converter_id,
                        article_id,
                        force_convert)
                    converter_id += 1

        elif split_map['split_type'] == 'medline':
            record_expr = re.compile(b'^(Record\s#\d+\sof\s)(\d+\s*)$')
            id_expr = re.compile(b'^(ID:\s)([A-Z0-9-]+)(\s*)$')
            newline_expr = re.compile(b'^[\r\n]+$')
            new_record, local_article_file = False, None

            with codecs.open(local_out_file, 'rb') as fi:
                for line in fi:
                    if converter_id == converter_batch_size:
                        converter_id = 1

                    detector = chardet.detect(line)
                    encoding = detector['encoding']
                    confidence = detector['confidence']

                    if encoding != 'ascii' and confidence == 1.0:
                        line = line.decode(encoding).encode('utf-8')

                    if not new_record and not local_article_file \
                            and re.findall(record_expr, line):
                        new_record = True
                        continue
                    elif new_record and not local_article_file:
                        matches = re.findall(id_expr, line)
                        if matches and len(matches[0]) == 3:
                            article_id = matches[0][1].decode('utf-8')
                            article_filename = '{}.txt'.format(article_id)
                            local_article_file = os.path.join(
                                local_out_split_dir,
                                article_filename)

                    if new_record and local_article_file:
                        if re.findall(newline_expr, line):
                            publish_article(
                                s3_client,
                                lf,
                                s3_out_files_dir_key,
                                local_article_file,
                                source_name,
                                bucket_name,
                                sns_topic_base_name,
                                converter_id,
                                article_id,
                                force_convert,
                                False)
                            converter_id += 1
                            new_record, local_article_file = False, None
                            continue
                        with open(local_article_file, 'ab+') as af:
                            af.write(line)

        elif split_map['split_type'] == 'json':
            with codecs.open(local_out_file, 'rb') as fi:
                articles = ijson.items(
                    fi, 'PubmedArticleSet.PubmedArticle.item')
                for idx, article in enumerate(articles):
                    article_processed, local_article_file = \
                        process_pubmed_article(
                            local_out_split_dir, article)
                    if article_processed:
                        s3_article_file = os.path.join(
                            s3_out_files_dir_key,
                            ntpath.basename(local_article_file))
                        s3_client.upload_file(
                            local_article_file,
                            bucket_name,
                            s3_article_file)
                        lf.write('{} - split article #{} available @ '
                                 '{}'.format(get_timestamp(2), idx,
                                             s3_article_file))
                        lf.write('\n')
                    else:
                        lf.write('{} - split article #{} failed'.format(
                            get_timestamp(2), idx))
                        lf.write('\n')
                    bulk_delete_files(local_article_file)

            shutil.rmtree(local_out_split_dir)

        lf.write('{} - Finished Split process for {}...'.format(
            get_timestamp(2),
            s3_raw_file))
        lf.write('\n')

    logger.debug('Finished Split process for {}...'.format(local_out_file))


def decompress(
        s3_client,
        s3_in_file,
        source_name,
        bucket_name,
        sns_topic_base_name,
        converter_batch_size):
    process_start_time = time.time()

    if not s3_in_file.endswith(('.gz', '.zip')):
        logger.debug('Skipping {} because it is not a'
                     ' compressed file'.format(s3_in_file))

    (s3_out_files_decompressed_dir_key,
     local_in_file,
     local_out_decompressed_gzip_file,
     local_out_decompressed_zip_dir,
     local_out_json_file,
     s3_in_file_decompression_process_log,
     local_in_file_decompression_process_log,
     local_out_decompressed_dir_all
     ) = setup_s3_file_level_nomenclature(
        s3_in_file,
        source_name)

    s3_client.download_file(
        bucket_name, s3_in_file, local_in_file)
    logger.debug('Downloaded compressed file {} to {}'.format(
        s3_in_file, local_in_file))
    local_in_file_size = os.stat(local_in_file).st_size

    if local_in_file.endswith('.gz'):
        gzip_extractor(
            local_in_file, local_out_decompressed_gzip_file)
        local_decompressed_files_iterator = iter([
            local_out_decompressed_gzip_file])
    elif local_in_file.endswith('.zip'):
        source_fileformat = source_fileformat_map.get(source_name)
        zip_extractor(
            local_in_file, local_out_decompressed_zip_dir)
        local_decompressed_files_iterator = (
            os.path.join(prefix, file
                         ) for prefix, subdirs, files in
            os.walk(local_out_decompressed_zip_dir
                    ) for file in files if should_extract(
                file, source_fileformat))

    skipped = 0
    converter_id = 1
    for idx, local_decompressed_file in \
            enumerate(local_decompressed_files_iterator):

        if converter_id == converter_batch_size:
            converter_id = 1

        if not is_valid_format(local_decompressed_file,
                               source_fileformat_map.get(source_name)):
            skipped += 1
            log_msg = 'Skipping due to invalid format: {}'.format(
                local_decompressed_file)
            logger.debug(log_msg)

            with open(local_in_file_decompression_process_log, 'a+') as lf:
                lf.write('{} - {}'.format(
                    get_timestamp(2), log_msg))
                lf.write('\n')
            os.remove(local_decompressed_file)

            continue

        if source_split_map[source_name]:
            file_split_and_upload(
                s3_client,
                bucket_name,
                source_name,
                s3_in_file,
                local_decompressed_file,
                local_out_decompressed_dir_all,
                s3_out_files_decompressed_dir_key,
                local_in_file_decompression_process_log,
                s3_in_file_decompression_process_log,
                sns_topic_base_name,
                converter_batch_size,
                True
            )
        else:
            if transform_name_to_uuid_map[source_name]:
                local_decompressed_file_basename = article_id = \
                    ntpath.basename(
                        local_decompressed_file)
                local_decompressed_file_basename_transformed = \
                    '{}.{}'.format(transform_name_to_uuid(
                        local_decompressed_file_basename),
                        source_fileformat_map[source_name])
                local_decompressed_file_dirname = os.path.dirname(
                    local_decompressed_file)
                local_decompressed_file_transformed = os.path.join(
                    local_decompressed_file_dirname,
                    local_decompressed_file_basename_transformed)
                os.rename(local_decompressed_file,
                          local_decompressed_file_transformed)
                local_decompressed_file = local_decompressed_file_transformed

            with open(local_in_file_decompression_process_log, 'a+') as lf:
                publish_article(
                    s3_client,
                    lf,
                    s3_out_files_decompressed_dir_key,
                    local_decompressed_file,
                    source_name,
                    bucket_name,
                    sns_topic_base_name,
                    converter_id,
                    article_id,
                    True,
                    remove_local_article=True)
            converter_id += 1

            # local_decompressed_file_basename = ntpath.basename(
            #     local_decompressed_file)
            # s3_decompressed_file = os.path.join(
            #     s3_out_files_decompressed_dir_key,
            #     local_decompressed_file_basename)

            # with open(local_in_file_decompression_process_log, 'a+') as lf:
            #     if s3_exists(s3_client, bucket_name,
            #                  s3_decompressed_file):
            #         lf.write('{} - Decompressed file'
            #                  ' for {} already exists '
            #                  '@ {}'.format(get_timestamp(2),
            #                                s3_in_file,
            #                                s3_decompressed_file))
            #     else:
            #         s3_client.upload_file(
            #             local_decompressed_file,
            #             bucket_name,
            #             s3_decompressed_file
            #         )
            #         lf.write('{} - Successfully decompressed {} '
            #                  'and uploaded to {}'.format(
            #                      get_timestamp(2),
            #                      s3_in_file, s3_decompressed_file))
            #     lf.write('\n')

            if post_decompress_move_map[source_name]:
                shutil.move(local_decompressed_file,
                            os.path.join(
                                local_out_decompressed_dir_all,
                                local_decompressed_file_basename))

    process_end_time = time.time()
    if local_in_file.endswith('.zip'):
        local_out_file_size = get_dir_tree_size(
            local_out_decompressed_zip_dir)
    elif local_in_file.endswith('.gz'):
        local_out_file_size = os.stat(local_out_decompressed_gzip_file
                                      ).st_size
    total_processing_time = process_end_time - process_start_time
    compressed_processing_speed = round(
        local_in_file_size / total_processing_time, 2)
    decompressed_processing_speed = round(
        local_out_file_size / total_processing_time, 2)

    with open(local_in_file_decompression_process_log, 'a+') as lf:
        if local_in_file.endswith('.zip'):
            log_msg = 'Total files extracted: {}'.format(idx + 1)
            logger.debug(log_msg)
            log_msg = '{} - {}'.format(get_timestamp(2), log_msg)
            lf.write(log_msg)
            lf.write('\n')

            log_msg = 'Total files processed: {}'.format(idx + 1 - skipped)
            logger.debug(log_msg)
            log_msg = '{} - {}'.format(get_timestamp(2), log_msg)
            lf.write(log_msg)
            lf.write('\n')

            log_msg = 'Total files skipped due to invalid format: {}'.format(
                skipped)
            logger.debug(log_msg)
            log_msg = '{} - {}'.format(get_timestamp(2), log_msg)
            lf.write(log_msg)
            lf.write('\n')

        log_msg = 'Input (compressed) file size: {}'.format(
            readable_size(local_in_file_size))
        logger.debug(log_msg)
        log_msg = '{} - {}'.format(get_timestamp(2), log_msg)
        lf.write(log_msg)
        lf.write('\n')

        log_msg = 'Input (decompressed) file size: {}'.format(
            readable_size(local_out_file_size))
        logger.debug(log_msg)
        log_msg = '{} - {}'.format(get_timestamp(2), log_msg)
        lf.write(log_msg)
        lf.write('\n')

        log_msg = 'Total run time: {}'.format(
            readable_time(total_processing_time))
        logger.debug(log_msg)
        log_msg = '{} - {}'.format(get_timestamp(2), log_msg)
        lf.write(log_msg)
        lf.write('\n')

        log_msg = 'Processing speed (based on decompressed input file size'
        '): {}'.format(readable_size(decompressed_processing_speed))
        logger.debug(log_msg)
        log_msg = '{} - {}'.format(get_timestamp(2), log_msg)
        lf.write(log_msg)
        lf.write('\n')

        log_msg = 'Processing speed (based on compressed input file size'
        '): {}'.format(readable_size(compressed_processing_speed))
        logger.debug(log_msg)
        log_msg = '{} - {}'.format(get_timestamp(2), log_msg)
        lf.write(log_msg)
        lf.write('\n')

    s3_client.upload_file(
        local_in_file_decompression_process_log,
        bucket_name,
        s3_in_file_decompression_process_log
    )
    logger.debug('Uploaded process log to {}'.format(
        s3_in_file_decompression_process_log))


def run_decompress(args):
    bucket_name = 'nexscope-safety'
    source_name = args.source
    s3_in_file = args.processfile
    process_date = args.processdate
    converter_batch_size = args.batchsize
    s3_client = get_s3_client()
    sns_topic_base_name = '{}{}-converter'.format(
        source_name, process_date.strftime('%Y%m%d'))

    decompress(
        s3_client,
        s3_in_file,
        source_name,
        bucket_name,
        sns_topic_base_name,
        converter_batch_size)


def run_deltacon(args):
    bucket_name = 'nexscope-safety'
    source_name = args.source
    s3_in_dir = args.processdir
    process_date = args.processdate
    converter_batch_size = args.batchsize
    strategy_name = args.strategyname
    s3_client = get_s3_client()
    sns_topic_base_name = '{}{}-converter'.format(
        source_name, process_date.strftime('%Y%m%d'))

    params = dict(
        Bucket=bucket_name,
        Prefix=s3_in_dir,
        PaginationConfig=dict(PageSize=converter_batch_size),
        Delimiter='')
    paginator = s3_client.get_paginator('list_objects')
    piterator = iter(paginator.paginate(**params))

    page_idx = 0
    still_more = True
    pages = None

    iterate = page_idx < pages and still_more if isinstance(
        pages, int) else still_more

    while iterate:
        try:
            current_page = next(piterator)
        except StopIteration:
            still_more = False
        else:
            for idx, item in enumerate(current_page['Contents']):
                if item['Key'].endswith(source_fileformat_map[source_name]):
                    deltacon(
                        source_name,
                        bucket_name,
                        item['Key'],
                        sns_topic_base_name,
                        idx + 1,
                        True,
                        strategy_name)
        iterate = page_idx < pages and still_more if isinstance(
            pages, int) else still_more


def get_args():
    parser = ArgumentParser()
    parser.add_argument('-s', '--source', nargs='?', help='medical source')
    parser.add_argument('-p', '--processdate', nargs='?',
                        type=lambda p: datetime.datetime.strptime(
                            p, '%Y-%m-%d'),
                        help='processing date -- format YYYY-MM-DD')
    parser.add_argument('-f', '--processfile', nargs='?',
                        help='compressed file name to process')
    parser.add_argument('-d', '--processdir', nargs='?',
                        help='decompressed dir to process for deltacon')
    parser.add_argument('-b', '--batchsize', nargs='?',
                        type=int,
                        help='equals number of sns topics')
    parser.add_argument('-t', '--strategyname',
                        nargs='?', help='strategy to use')

    return parser.parse_args()


def main():
    args = get_args()
    # run_decompress(args)
    run_deltacon(args)
    shutil.rmtree(temp_dir)


if __name__ == '__main__':
    main()
