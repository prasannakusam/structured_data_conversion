import json
import xml.etree.cElementTree as ElementTree
from collections import defaultdict
from argparse import ArgumentParser
from boto3.session import Session

from lshared import readable_time
from saxp36 import XmlToJson
from itertools import chain
from ftplib import (
    FTP,
    error_temp,
    error_perm
)
import rapidjson
import xmltodict
import jxmlease
import datetime
import tempfile
import logging
import zipfile
import pathlib
import difflib
import codecs
import shutil
import ntpath
import ijson
import time
import gzip
import sys
import csv
import os



root_dir = os.path.join(os.path.expanduser('~'), 'data')
output_log_dir = os.path.join(os.path.expanduser('~'), 'logs')
output_log_file = os.path.join(output_log_dir, 'output.{}.log'.format(
    datetime.datetime.now().strftime('%Y%m%d%H%M%S')))
output_csv_file = os.path.join(output_log_dir, 'metrics.{}.csv'.format(
    datetime.datetime.now().strftime('%Y%m%d%H%M%S')))
# bucket_1 = 'mmcapturepoctemp'
# bucket_2 = 'xconverter'
bucket_1 = 'nexscope-safety-va'
temp_dir = tempfile.mkdtemp(dir='/tmp/processing/')
# temp_dir = tempfile.mkdtemp(dir='/processing')
strategy_runner_times = dict()
# sample_topic = 'arn:aws:sns:us-east-2:812531199550:Pubmed-Sample-Test'

source_article_split_params_map = {
    'pubmed': {
        'split_type': 'xml',
        'article_start_tag': 'PubmedArticle',
        'article_id_tag': './MedlineCitation/PMID',
        'article_id_tag_items': [
            [('Version', '1')],
            [('Version', '2')],
            [('Version', '3')],
            [('Version', '4')]
        ]
    },
    'who': {
        'split_type': 'xml',
        'article_start_tag': 'doc',
        'article_id_tag': 'str',
        'article_id_tag_items': [[('name', 'id')]]
    },
    'cochrane': {
        'split_type': 'medline',
    }
}
json_modifier_map = {
    'pubmed': False,
    'cochrane': False,
    'who': True,
    'drugbank': False,
    'dailymed': False,
    'nct': False
}
post_decompress_move_map = {
    'pubmed': False,
    'cochrane': False,
    'who': False,
    'drugbank': True,
    'dailymed': True,
    'nct': True
}
source_split_map = {
    'pubmed': True,
    'cochrane': True,
    'who': True,
    'drugbank': False,
    'dailymed': False,
    'nct': False
}
source_fileformat_map = {
    'pubmed': 'xml',
    'cochrane': 'txt',
    'who': 'xml',
    'drugbank': 'json',
    'dailymed': 'xml',
    'nct': 'xml'
}
source_conversion_map = {
    'pubmed': True,
    'cochrane': True,
    'who': True,
    'drugbank': False,
    'dailymed': True,
    'nct': True
}


class Error(Exception):
    pass


class KeyNotFoundError(Error):
    def __init__(self, msg):
        self.message = msg


class NestedDict(dict):
    """Nested Dictionary class

    """

    def __getitem__(self, item):
        try:
            return dict.__getitem__(self, item)
        except KeyError:
            value = self[item] = type(self)()
            return value


def get_aws_session(new=False):
    if new:
        aws_access_key_id = 'AKIAJLRXYJPUIWI6P3LA'
        aws_secret_access_key = \
            '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'
        region_name = 'us-east-1'
    else:
        aws_access_key_id = 'AKIAIACV4OX542PSK4HA'
        aws_secret_access_key = \
            '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'
        region_name = 'us-east-2'

    session = Session(
        aws_access_key_id=aws_access_key_id,
        aws_secret_access_key=aws_secret_access_key,
        region_name=region_name
    )

    return session


def get_s3_client(new=False):
    session = get_aws_session(new)

    return session.client('s3')


def get_sns_client(new=False):
    session = get_aws_session(new)

    return session.client('sns')


def get_sqs_client(new=False):
    session = get_aws_session(new)

    return session.client('sqs')


def get_lambda_client(new=False):
    session = get_aws_session(new)

    return session.client('lambda')


def get_logger(filename, log_file=None):
    logger = logging.getLogger(filename)
    logger.setLevel(logging.DEBUG)

    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    channel = logging.FileHandler(log_file) if log_file else \
        logging.StreamHandler(sys.stdout)
    channel.setLevel(logging.DEBUG)
    channel.setFormatter(formatter)

    logger.addHandler(channel)

    return logger


logger = get_logger(__file__)


# def escape(ipstr):
#     ipstr = ipstr.replace("&", "&amp;")
#     ipstr = ipstr.replace("p<0.05", "p=0.05")
#     ipstr = ipstr.replace("p<0,", "p=0.")
#     ipstr = ipstr.replace("p < 0,", "p=0.")
#     ipstr = ipstr.replace("p <0,", "p=0.")
#
#     return str
#
#
# def timefunc(f):
#     def f_timer(*args, **kwargs):
#         start = time.time()
#         result = f(*args, **kwargs)
#         end = time.time()
#         time_taken = end - start
#         strategy_runner_times[args[0]] = time_taken
#         logger.debug('Strategy Runner for '
#                      'Strategy#{} took: {} seconds'.format(
#                          args[0], time_taken))
#         return result
#     return f_timer
#
#
# def size_formatter(value, unit, is_speed=False):
#     out_unit = unit if not is_speed else '{}/s'.format(unit)
#
#     return '{} {}'.format(value, out_unit)
#
#
# def seconds_parser(seconds):
#     minutes, seconds = divmod(seconds, 60)
#     hours, minutes = divmod(minutes, 60)
#     days, hours = divmod(hours, 24)
#
#     return days, hours, minutes, seconds
#
#
# def readable_time(value):
#     days, hours, minutes, seconds = seconds_parser(value)
#
#     readable_time_str = ''
#
#     if days:
#         days_str = 'days' if days > 1 else 'day'
#         readable_time_str = '{}{} {} '.format(
#             readable_time_str, int(days), days_str)
#     if hours:
#         hours_str = 'hrs' if hours > 1 else 'hr'
#         readable_time_str = '{}{} {} '.format(
#             readable_time_str, int(hours), hours_str)
#     if minutes:
#         minutes_str = 'mins' if minutes > 1 else 'min'
#         readable_time_str = '{}{} {} '.format(
#             readable_time_str, int(minutes), minutes_str)
#     readable_time_str = '{}{} secs'.format(
#         readable_time_str, round(seconds, 2))
#
#     return readable_time_str
#
#
# def readable_size(value, is_speed=False):
#     kilos = 1024
#     megs = kilos * 1024
#     gigs = megs * 1024
#     teras = gigs * 1024
#
#     if value >= teras:
#         return size_formatter(round(value/teras, 2), 'TB', is_speed)
#     elif value < teras and value >= gigs:
#         return size_formatter(round(value/gigs, 2), 'GB', is_speed)
#     elif value < gigs and value >= megs:
#         return size_formatter(round(value/megs, 2), 'MB', is_speed)
#     elif value < megs and value >= kilos:
#         return size_formatter(round(value/kilos, 2), 'KB', is_speed)
#     else:
#         return size_formatter(value, 'bytes', is_speed)


def xml_element_to_dict(elem):
    """
    Convert XML Element to a simple dict

    """
    logger.debug(elem.attrib)
    inner = dict(elem.attrib)
    inner = dict([(k, [v]) for k, v in inner.items()])
    children = list(map(xml_element_to_dict, elem.getchildren()))
    text = elem.text and elem.text.strip()
    if text:
        inner['text'] = [text]
    if children:
        inner['children'] = children

    return {elem.tag: [inner]}


def s3_exists(client, bucket, key):
    resp = client.list_objects(
        Bucket=bucket,
        Prefix=key)
    expected_object = resp.get('Contents', [])

    return bool(expected_object)


def fetch_filename_stem(filename):
    stem = filename
    suffix = True
    while suffix:
        resolver = pathlib.Path(stem)
        stem = resolver.stem
        suffix = resolver.suffix

    return stem


def get_timestamp(formatter_type=1):
    if formatter_type == 1:
        return datetime.datetime.now().strftime('%Y%m%d%H%M%S')
    elif formatter_type == 2:
        return datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')


def s3_download_file(s3_file_key, bucket_name=None, output_dir=None):
    s3_client = get_s3_client(True)
    if not bucket_name:
        bucket_name = bucket_1
    local_file = ntpath.basename(s3_file_key)
    if output_dir:
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        local_file = os.path.join(output_dir, local_file)

    s3_client.download_file(
        bucket_name,
        s3_file_key,
        local_file)
    return local_file


def zip_extractor(infile, out_dir):
    if not zipfile.is_zipfile(
            infile) or not infile.endswith('.zip'):
        return

    with zipfile.ZipFile(infile, 'r') as inzip:
        file_list = inzip.namelist()
        inzip.extractall(out_dir)
        file_list = [os.path.join(out_dir, f) for f in file_list]

    os.remove(infile)

    for filename in filter(zipfile.is_zipfile, file_list):
        zip_extractor(filename, out_dir)


def gzip_extractor(infile, outfile):
    with gzip.open(infile, 'rb') as inf:
        with open(outfile, 'wb') as otf:
            shutil.copyfileobj(inf, otf)


def setup_s3_dir_level_nomenclature(s3_in_files_dir_key, source_name):
    current_timestamp = get_timestamp()
    s3_in_files_process_log_dir_key = s3_in_files_dir_key.replace(
        'data', 'logs')
    s3_in_files_process_log_dir_key_list = \
        s3_in_files_process_log_dir_key.split(os.sep)
    s3_in_files_process_log_dir_key_list.remove('raw')
    s3_in_files_process_log_dir_key = os.sep.join(
        s3_in_files_process_log_dir_key_list)
    s3_out_files_dir_key = s3_in_files_dir_key.replace('raw', 'processed')
    s3_out_files_dir_key_list = s3_out_files_dir_key.split(os.sep)
    s3_out_files_dir_key_source_name_index = \
        s3_out_files_dir_key_list.index(source_name)
    s3_out_files_dir_key_list = s3_out_files_dir_key_list[
        0: s3_out_files_dir_key_source_name_index + 1]
    source_fileformat = source_fileformat_map.get(source_name)
    s3_out_files_decompressed_dir_key = os.sep.join(list(chain(
        s3_out_files_dir_key_list, ['decompressed'])))
    s3_out_files_json_dir_key = os.sep.join(list(chain(
        s3_out_files_dir_key_list, ['deltacon'])))
    local_out_decompressed_dir_all = os.path.join(
        temp_dir,
        'decompressed',
        'all_{}'.format(current_timestamp))
    decompression_process_log_basename = '.'.join((
        'decompression_{}'.format(current_timestamp), 'log'))
    deltacon_process_log_basename = '.'.join((
        'deltacon_{}'.format(current_timestamp), 'log'))
    s3_in_file_decompression_process_log = os.path.join(
        s3_in_files_process_log_dir_key,
        decompression_process_log_basename)
    local_in_file_decompression_process_log = os.path.join(
        temp_dir, decompression_process_log_basename)
    s3_in_file_deltacon_process_log = os.path.join(
        s3_in_files_process_log_dir_key,
        deltacon_process_log_basename)
    local_in_file_deltacon_process_log = os.path.join(
        temp_dir, deltacon_process_log_basename)

    if not os.path.exists(local_out_decompressed_dir_all):
        os.makedirs(local_out_decompressed_dir_all)

    return (
        s3_in_file_decompression_process_log,
        local_in_file_decompression_process_log,
        s3_in_file_deltacon_process_log,
        local_in_file_deltacon_process_log,
        s3_out_files_decompressed_dir_key,
        s3_out_files_json_dir_key,
        local_out_decompressed_dir_all
    )


def setup_s3_file_level_nomenclature(
        s3_in_file,
        source_name,
        local_out_decompressed_dir_all):
    s3_in_file_dir_key = os.path.dirname(s3_in_file)
    s3_in_file_basename = ntpath.basename(s3_in_file)
    s3_in_filename = fetch_filename_stem(s3_in_file_basename)
    source_fileformat = source_fileformat_map.get(source_name)
    local_in_file = os.path.join(
        temp_dir, ntpath.basename(s3_in_file_basename))
    local_out_decompressed_gzip_file_basename = '.'.join((
        s3_in_filename,
        source_fileformat))
    local_out_decompressed_gzip_file = os.path.join(
        local_out_decompressed_dir_all,
        local_out_decompressed_gzip_file_basename)
    local_out_decompressed_zip_dir = os.path.join(
        temp_dir,
        s3_in_filename)

    if not os.path.exists(local_out_decompressed_zip_dir):
        os.makedirs(local_out_decompressed_zip_dir)

    local_out_json_file_basename = '.'.join((s3_in_filename, 'json'))
    local_out_json_file = os.path.join(temp_dir, local_out_json_file_basename)

    return (s3_in_file,
            local_in_file,
            local_out_decompressed_gzip_file,
            local_out_decompressed_zip_dir,
            local_out_json_file)


def process_pubmed_article(out_dir, article):
    processed = True
    article_file = None

    if not os.path.exists(out_dir):
        os.makedirs(out_dir)

    try:
        article = dict(PubmedArticle=article)
        article_file = os.path.join(
            out_dir,
            'pubmed_article_{}.json'.format(
                article['PubmedArticle'][
                    'MedlineCitation']['PMID']['text']))

        with open(article_file, 'w', encoding='UTF-8') as fo:
            rapidjson.dump(article, fo)

    except Exception as e:
        processed = False

    return processed, article_file


# def extract_article_id(article_elem, article_id_tag, article_id_tag_items=[]):
#     found_id = False
# 
#     while not found_id:
#         found_id = article_elem.tag == article_id_tag and \
#             article_elem.items() in article_id_tag_items if \
#             article_id_tag_items else article_elem.tag == article_id_tag
#         if not found_id:
#             article_elem = next(iter(article_elem))
# 
# 
#     text = article_elem.text
#     print(text)
# 
#     return article_elem.text


class RunTimeError(Exception):
    pass


def extract_article_id(article_elem, article_id_tag, article_id_tag_items):
    try:
        for element in article_elem.findall(article_id_tag):
            if element.items() in article_id_tag_items:
                return element.text
    except Exception as e:
        logger.debug("Article ID not found with exception: {}".format(e))

    # raise RunTimeError('Article ID not found')


def file_split_and_upload(
        s3_client,
        bucket_name,
        source_name,
        s3_raw_file,
        local_out_file,
        local_out_split_dir,
        s3_out_files_dir_key,
        local_in_file_process_log,
        s3_in_file_process_log,
        remove_local_out_file=False
):
    logger.debug('Starting Split process for {}...'.format(
        local_out_file))

    split_map = source_article_split_params_map[source_name]

    with open(local_in_file_process_log, 'a+') as lf:
        lf.write('{} - Starting Split process for {}...'.format(
            get_timestamp(2),
            s3_raw_file))
        lf.write('\n')

        if split_map['split_type'] == 'xml':
            context = ElementTree.iterparse(local_out_file, events=("start",))
            count = 1
            for event, element in context:

                if element.tag == split_map['article_start_tag']:
                    count += 1
                    article_id = extract_article_id(
                        element, split_map['article_id_tag'],
                        split_map['article_id_tag_items'])
                    if article_id is not None:
                        article_filename = '{}_article_{}.xml'.format(
                            source_name,
                            article_id)
                        local_article_file = os.path.join(local_out_split_dir,
                                                          article_filename)

                        with open(local_article_file, 'wb') as af:
                            af.write('<?xml version="1.0" '
                                     'encoding="UTF-8"?>\n'.encode(
                                         'utf8'))
                            af.write(ElementTree.tostring(element))
                        s3_article_file = os.path.join(
                            s3_out_files_dir_key,
                            ntpath.basename(local_article_file))
                        s3_client.upload_file(
                            local_article_file,
                            bucket_name,
                            s3_article_file)
                        lf.write('{} - split article #{} available @ '
                                 '{}'.format(get_timestamp(2), article_id,
                                             s3_article_file))
                        lf.write('\n')
                    else:
                        logger.debug('Article Id not found in split #: {} in file: {}'.format(count, local_out_file))

        elif split_map['split_type'] == 'json':
            with codecs.open(local_out_file, 'rb') as fi:
                articles = ijson.items(
                    fi, 'PubmedArticleSet.PubmedArticle.item')

                for idx, article in enumerate(articles):
                    article_processed, local_article_file = \
                        process_pubmed_article(
                            local_out_split_dir, article)
                    if article_processed:
                        s3_article_file = os.path.join(
                            s3_out_files_dir_key,
                            ntpath.basename(local_article_file))
                        s3_client.upload_file(
                            local_article_file,
                            bucket_name,
                            s3_article_file)
                        lf.write('{} - split article #{} available @ '
                                 '{}'.format(get_timestamp(2), idx,
                                             s3_article_file))
                        lf.write('\n')
                    else:
                        lf.write('{} - split article #{} failed'.format(
                            get_timestamp(2), idx))
                        lf.write('\n')
                    bulk_delete_files(local_article_file)

            shutil.rmtree(local_out_split_dir)

        lf.write('{} - Finished Split process for {}...'.format(
            get_timestamp(2),
            s3_raw_file))
        lf.write('\n')

    if remove_local_out_file:
        bulk_delete_files(local_out_file)

    logger.debug('Finished Split process for {}...'.format(local_out_file))


def list_dir(
        s3_client,
        bucket_name,
        prefix,
        page_limit,
        pages=None,
        delimiter='',
        file_writer=None,
        write_file_name_only=False,
        compare_hash=False,
        count_only=False):

    params = dict(
        Bucket=bucket_name,
        Prefix=prefix,
        PaginationConfig=dict(PageSize=page_limit),
        Delimiter=delimiter)
    paginator = s3_client.get_paginator('list_objects')
    piterator = iter(paginator.paginate(**params))

    page_idx = 0
    all_keys = []
    still_more = True
    total_objects = 0

    iterate = page_idx < pages and still_more if isinstance(
        pages, int) else still_more

    while iterate:
        try:
            current_page = next(piterator)
        except StopIteration:
            still_more = False
        else:
            if file_writer:
                for item in current_page['Contents']:
                    if write_file_name_only:
                        file_writer.write(ntpath.basename(item['Key']))
                    else:
                        if compare_hash:
                            file_writer.write('{}\t{}\t{}'.format(
                                ntpath.basename(item['Key']),
                                item['ETag'],
                                item['Size']))
                        else:
                            file_writer.write('{}\t{}'.format(
                                ntpath.basename(item['Key']),
                                item['Size']))
                    file_writer.write('\n')
                    total_objects += 1
            else:
                total_objects += len(current_page['Contents'])
                all_keys.append((item['Key'] for item in current_page[
                    'Contents']))
            page_idx += 1
        iterate = page_idx < pages and still_more if isinstance(
            pages, int) else still_more

    if count_only:
        return total_objects
    if not all_keys:
        return all_keys, total_objects
    return chain.from_iterable(all_keys), total_objects


def download_from_pubmed_to_s3():
    input_xml_dir = os.path.join(root_dir, 'inputs', 'xml')
    s3_client = get_s3_client()

    ftp_root = 'ftp.ncbi.nlm.nih.gov'
    ftp_paths = [
        '/pubmed/baseline',
        '/pubmed/updatefiles',
    ]

    for ftp_path in ftp_paths:
        local_ftp_dir = os.path.join(input_xml_dir, *ftp_path.split(os.sep))
        local_ftp_dir_list = local_ftp_dir.split(os.sep)
        s3_in_file_dir_key = os.path.join(*local_ftp_dir_list[
            local_ftp_dir_list.index('inputs'):])

        if not os.path.exists(local_ftp_dir):
            os.makedirs(local_ftp_dir)

        ftp_client = FTP(ftp_root)
        ftp_client.login()
        ftp_client.cwd(ftp_path)

        for idx, gfile in enumerate(ftp_client.nlst()):
            if gfile.endswith('.xml.gz'):
                xfile = gfile.replace('.gz', '')
                local_ftp_gzip_file = os.path.join(local_ftp_dir, gfile)
                local_ftp_xml_file = os.path.join(
                    local_ftp_dir, xfile)
                s3_file_key = os.path.join(s3_in_file_dir_key, xfile)

                if not s3_exists(s3_client, bucket_1, s3_file_key):
                    upload_to_s3 = True

                    if not os.path.exists(local_ftp_xml_file):
                        with open(local_ftp_gzip_file, 'wb') as lf:
                            try:
                                ftp_client.retrbinary(
                                    'RETR {}'.format(gfile), lf.write)
                            except (error_temp, error_perm):
                                upload_to_s3 = False
                                logger.debug('Unable to download'
                                             ' {}. Skipping...'.format(
                                                 gfile))
                            else:
                                with gzip.open(local_ftp_gzip_file,
                                               'rb') as gf:
                                    with open(local_ftp_xml_file, 'wb') as xf:
                                        shutil.copyfileobj(gf, xf)
                                os.remove(local_ftp_gzip_file)

                    if upload_to_s3:
                        s3_client.upload_file(
                            local_ftp_xml_file,
                            bucket_1,
                            s3_file_key)
                        os.remove(local_ftp_xml_file)

                        logger.debug('Moved {} to S3'.format(
                            local_ftp_xml_file))
                else:
                    logger.debug('{} is already available in S3'.format(
                        local_ftp_xml_file))


def is_empty_file(xfile):
    return os.stat(xfile).st_size == 0


def bulk_delete_files(xfiles):
    if not isinstance(xfiles, list):
        xfiles = [xfiles]

    for xfile in xfiles:
        if os.path.exists(xfile):
            os.remove(xfile)


def strategy1(input_file, output_file):
    """
    `xmltodict` library based strategy

    """
    with open(output_file, 'w', encoding='UTF-8') as fo:
        with codecs.open(input_file, 'rb') as fi:
            rapidjson.dump(xmltodict.parse(
                fi,
                # dict_constructor=lambda *args, **kwargs: 
                #     defaultdict(list, *args, **kwargs),
                # force_list=('title'),
                attr_prefix='',
                cdata_key='text'), fo, ensure_ascii=False)


def strategy2(input_file, output_file):
    """
    sax based strategy (old-style)

    """
    p = XmlToJson(input=input_file, output=output_file)
    p.parse()


def strategy3(input_file, output_file):
    """
    `jxmlease` library based strategy

    """
    with open(output_file, 'w') as fo:
        with codecs.open(input_file, 'rb') as fi:
            rapidjson.dump(jxmlease.parse(fi), fo)


def strategy4(input_file, output_file):
    """
    `etree` based strategy

    """
    xml_parser = ElementTree.parse(input_file)

    root_dir = xml_parser.getroot()

    with open(output_file, 'w') as fo:
        rapidjson.dump(xml_element_to_dict(root_dir), fo)


def delta_gen(diff_file):
    with open(diff_file, 'r') as df:
        for line in df.readlines():
            if line.startswith('+++'):
                continue
            if line.startswith('+'):
                yield line.split('\t')[0].replace('+', '').strip()


def decompressor(
        s3_client,
        s3_in_files_dir_key,
        bucket_name,
        contents,
        source_name,
        ignore_log=False,
        upload_log=True):
    decompression_start = time.time()

    (s3_in_file_decompression_process_log,
     local_in_file_decompression_process_log,
     s3_in_file_deltacon_process_log,
     local_in_file_deltacon_process_log,
     s3_out_files_decompressed_dir_key,
     s3_out_files_json_dir_key,
     local_out_decompressed_dir_all) = setup_s3_dir_level_nomenclature(
        s3_in_files_dir_key, source_name)

    if s3_exists(s3_client, bucket_name,
                 s3_in_file_decompression_process_log) and not ignore_log:
        logger.debug('Skipping decompression of {} because a decompression'
                     ' log already exists at {}'.format(
                         s3_in_files_dir_key,
                         s3_in_file_decompression_process_log))
        return

    logger.debug(
        'Local decompression process log may be available at {}'.format(
            local_in_file_decompression_process_log))

    for content in contents:
        content_key = content['Key']

        if not content_key.endswith((
                '.gz', '.zip')):
            logger.debug('Skipping {} because it is not a'
                         ' compressed file'.format(content_key))
            continue

        (s3_in_file,
         local_in_file,
         local_out_decompressed_gzip_file,
         local_out_decompressed_zip_dir,
         local_out_json_file
         ) = setup_s3_file_level_nomenclature(
            content_key, source_name, local_out_decompressed_dir_all)

        s3_client.download_file(bucket_name, s3_in_file, local_in_file)

        if local_in_file.endswith('.gz'):
            gzip_extractor(
                local_in_file, local_out_decompressed_gzip_file)
            local_decompressed_files_iterator = iter([
                local_out_decompressed_gzip_file])
        elif local_in_file.endswith('.zip'):
            zip_extractor(
                local_in_file, local_out_decompressed_zip_dir)
            local_decompressed_files_iterator = (
                os.path.join(prefix, file
                             ) for prefix, subdirs, files in
                os.walk(local_out_decompressed_zip_dir
                        ) for file in files if not file.startswith(
                    '.') and file.endswith(
                    source_fileformat_map.get(source_name))
            )

        for local_decompressed_file in \
                local_decompressed_files_iterator:
            if source_split_map[source_name]:
                file_split_and_upload(
                    s3_client,
                    bucket_name,
                    source_name,
                    s3_in_file,
                    local_decompressed_file,
                    local_out_decompressed_dir_all,
                    s3_out_files_decompressed_dir_key,
                    local_in_file_decompression_process_log,
                    s3_in_file_decompression_process_log,
                    True
                )
            else:
                local_decompressed_file_basename = ntpath.basename(
                    local_decompressed_file)
                s3_decompressed_file = os.path.join(
                    s3_out_files_decompressed_dir_key,
                    local_decompressed_file_basename)

                with open(local_in_file_decompression_process_log, 'a+') as lf:
                    if s3_exists(s3_client, bucket_name,
                                 s3_decompressed_file):
                        lf.write('{} - Decompressed file'
                                 ' for {} already exists '
                                 '@ {}'.format(get_timestamp(2),
                                               s3_in_file,
                                               s3_decompressed_file))
                    else:
                        s3_client.upload_file(
                            local_decompressed_file,
                            bucket_name,
                            s3_decompressed_file
                        )
                        lf.write('{} - Successfully decompressed {} '
                                 'and uploaded to {}'.format(
                                     get_timestamp(2),
                                     s3_in_file, s3_decompressed_file))
                    lf.write('\n')

                if post_decompress_move_map[source_name]:
                    shutil.move(local_decompressed_file,
                                os.path.join(
                                    local_out_decompressed_dir_all,
                                    local_decompressed_file_basename))

    decompression_end = time.time()

    total_decompression_time = decompression_end - decompression_start

    with open(local_in_file_decompression_process_log, 'a+') as lf:
        lf.write('{} - Total decompression time for {} is {}'.format(
            get_timestamp(2),
            s3_in_file,
            readable_time(total_decompression_time)))
        lf.write('\n')

    if upload_log:
        s3_client.upload_file(
            local_in_file_decompression_process_log,
            bucket_name,
            s3_in_file_decompression_process_log
        )

    logger.debug('Uploaded decompression log for {} to {}'.format(
        s3_in_files_dir_key,
        s3_in_file_decompression_process_log))

    return (
        s3_in_file_deltacon_process_log,
        local_in_file_deltacon_process_log,
        s3_out_files_decompressed_dir_key,
        s3_out_files_json_dir_key,
        local_out_decompressed_dir_all
    )


def publish_to_sns(
        topic_arn,
        message_dict,
):
    sns_client = get_sns_client()
    response = sns_client.publish(
        TargetArn=topic_arn,
        Message=json.dumps({'default': json.dumps(message_dict)}),
        MessageStructure='json'
    )


def deltacon(
        s3_client,
        bucket_name,
        strategy,
        source_name,
        s3_in_file_deltacon_process_log,
        local_in_file_deltacon_process_log,
        s3_out_files_decompressed_dir_key,
        s3_out_files_json_dir_key,
        local_out_decompressed_dir_all,
        comparison_s3_dir=None,
        ignore_log=False,
        force_convert=False):
    if s3_exists(s3_client, bucket_name,
                 s3_in_file_deltacon_process_log) and not ignore_log:
        logger.debug('Skipping deltacon process of {} because a delta'
                     ' conversion log already exists at {}'.format(
                         s3_out_files_decompressed_dir_key,
                         s3_in_file_deltacon_process_log))
        return

    logger.debug(
        'Local deltacon process log may be available at {}'.format(
            local_in_file_deltacon_process_log))

    if comparison_s3_dir:
        s3_file1, s3_file2, diff_file, diff_obj = calculate_delta(
            s3_client,
            bucket_name,
            comparison_s3_dir,
            s3_out_files_decompressed_dir_key)

        logger.debug('diff file for {} and {} is available at {}'.format(
            comparison_s3_dir,
            s3_out_files_decompressed_dir_key,
            diff_file))

        if is_empty_file(diff_file):
            logger.debug('No delta found. diff file {} is empty'.format(
                diff_file))
            with open(local_in_file_deltacon_process_log, 'w') as lf:
                lf.write('{} - No delta found between {} and {}'.format(
                    get_timestamp(2),
                    s3_out_files_decompressed_dir_key,
                    comparison_s3_dir))
                lf.write('\n')
            s3_client.upload_file(
                local_in_file_deltacon_process_log,
                bucket_name,
                s3_in_file_deltacon_process_log
            )
            logger.debug('Uploaded deltacon process log to {}'.format(
                s3_in_file_deltacon_process_log))

            return

        local_decompressed_files_iterator = (os.path.join(
            local_out_decompressed_dir_all, delta_file) for delta_file in
            delta_gen(diff_file))

    else:
        local_decompressed_files_iterator = (
            os.path.join(prefix, file
                         ) for prefix, subdirs, files in
            os.walk(local_out_decompressed_dir_all
                    ) for file in files if not
            file.startswith('.') and file.endswith(
                source_fileformat_map.get(
                    source_name))
        )

    for local_decompressed_file in \
            local_decompressed_files_iterator:
        local_decompressed_file_dirname = os.path.dirname(
            local_decompressed_file)
        local_decompressed_file_basename = ntpath.basename(
            local_decompressed_file)
        local_decompressed_filename_stem = fetch_filename_stem(
            local_decompressed_file_basename)
        local_out_json_file = os.path.join(
            local_decompressed_file_dirname,
            '.'.join((local_decompressed_filename_stem,
                      'json')))
        s3_out_json_file_key = os.path.join(
            s3_out_files_json_dir_key,
            ntpath.basename(local_out_json_file))
        s3_decompressed_file = os.path.join(
            s3_out_files_decompressed_dir_key,
            local_decompressed_file_basename
        )

        if s3_exists(s3_client, bucket_name,
                     s3_out_json_file_key) and not force_convert:
            log_msg = '{} -- Skipping deltacon of {} because its JSON '\
                'counterpart already exists at {}'.format(
                    get_timestamp(2),
                    s3_decompressed_file,
                    s3_out_json_file_key)
            with open(local_in_file_deltacon_process_log, 'a+') as lf:
                lf.write(log_msg)
                lf.write('\n')
            logger.debug(log_msg)
            os.remove(local_decompressed_file)

            continue

        if not os.path.exists(local_decompressed_file):
            s3_client.download_file(
                bucket_name,
                s3_decompressed_file,
                local_decompressed_file
            )

        if not source_conversion_map[source_name]:
            s3_client.upload_file(
                local_decompressed_file,
                bucket_name,
                s3_out_json_file_key
            )
            with open(local_in_file_deltacon_process_log, 'a+') as lf:
                lf.write('{} - Successufully copied {} to {} because no'
                         ' conversion is required'.format(
                             get_timestamp(2),
                             s3_decompressed_file,
                             s3_out_json_file_key))
                lf.write('\n')

            bulk_delete_files([
                local_decompressed_file])

            continue

        try:
            logger.debug(
                'Converting {}...'.format(
                    local_decompressed_file))
            strategy(
                local_decompressed_file,
                local_out_json_file)
        except Exception as e:
            with open(local_in_file_deltacon_process_log, 'a+') as lf:
                lf.write('{} - Failed converting {}'.format(
                    get_timestamp(2),
                    s3_decompressed_file))
                lf.write('\n')
            logger.debug(
                'Failed converting {} due to\n{}'.format(
                    s3_decompressed_file, e))
        else:
            logger.debug('Success converting {} (locally at {}) to {}'.format(
                s3_decompressed_file,
                local_decompressed_file,
                local_out_json_file))

        if json_modifier_map[source_name]:
            json_modifier(local_out_json_file)

        s3_client.upload_file(
            local_out_json_file,
            bucket_name,
            s3_out_json_file_key
        )
        with open(local_in_file_deltacon_process_log, 'a+') as lf:
            lf.write('{} - Successufully converted {} and '
                     'uploaded to {}'.format(
                         get_timestamp(2),
                         s3_decompressed_file,
                         s3_out_json_file_key))
            lf.write('\n')

        bulk_delete_files([
            local_decompressed_file,
            local_out_json_file])

    s3_client.upload_file(
        local_in_file_deltacon_process_log,
        bucket_name,
        s3_in_file_deltacon_process_log
    )
    logger.debug('Uploaded deltacon process log to {}'.format(
        s3_in_file_deltacon_process_log))


# def lambda_deltacon(
#         s3_client,
#         bucket_name,
#         strategy,
#         source_name,
#         s3_out_files_decompressed_dir_key,
#         s3_out_files_json_dir_key,
#         comparison_s3_dir=None,
#         ignore_log=False,
#         force_convert=False):
#     sns_topic_arn = 'arn:aws:sns:us-east-2:'\
#         '812531199550:Pubmed-Sample-Test'
#     if comparison_s3_dir:
#         s3_file1, s3_file2, diff_file, diff_obj = calculate_delta(
#             s3_client,
#             bucket_name,
#             comparison_s3_dir,
#             s3_out_files_decompressed_dir_key)
#
#         logger.debug('diff file for {} and {} is available at {}'.format(
#             comparison_s3_dir,
#             s3_out_files_decompressed_dir_key,
#             diff_file))
#
#         if is_empty_file(diff_file):
#             logger.debug('No delta found. diff file {} is empty'.format(
#                 diff_file))
#
#             return
#
#         s3_decompressed_files_iterator = (os.path.join(
#             s3_out_files_decompressed_dir_key, delta_file) for delta_file in
#             delta_gen(diff_file))
#     else:
#         params = dict(
#             Bucket=bucket_name,
#             Prefix=prefix,
#             PaginationConfig=dict(PageSize=page_limit),
#             Delimiter=delimiter)
#         paginator = s3_client.get_paginator('list_objects')
#         piterator = iter(paginator.paginate(**params))
#
#         page_idx = 0
#         all_keys = []
#         still_more = True
#         total_objects = 0
#
#         iterate = page_idx < pages and still_more if isinstance(
#             pages, int) else still_more
#
#         while iterate:
#             try:
#                 current_page = next(piterator)
#             except StopIteration:
#                 still_more = False
#             else:
#                 for item in current_page['Contents']:
#                     s3_decompressed_file = item['Key']
#                     s3_decompressed_file_basename = ntpath.basename(
#                         s3_decompressed_file)
#                     s3_decompressed_filename_stem = fetch_filename_stem(
#                         s3_decompressed_file_basename)
#                     s3_out_json_file_basename = '.'.join(
#                         (s3_decompressed_filename_stem,
#                          'json'))
#                     s3_out_json_file_key = os.path.join(
#                         s3_out_files_json_dir_key,
#                         s3_out_json_file_basename
#                     )
#
#                     publish_to_sns(
#                         sns_topic_arn,
#                         dict(
#                             infile_basename=s3_decompressed_file_basename,
#                             outfile_basename=s3_out_json_file_basename,
#                             s3_in_key=s3_decompressed_file,
#                             s3_out_key=s3_out_json_file_key)
#                     )
#                 page_idx += 1
#             iterate = page_idx < pages and still_more if isinstance(
#                 pages, int) else still_more
#
#     for local_decompressed_file in \
#             local_decompressed_files_iterator:
#         local_decompressed_file_dirname = os.path.dirname(
#             local_decompressed_file)
#         local_decompressed_file_basename = ntpath.basename(
#             local_decompressed_file)
#         local_decompressed_filename_stem = fetch_filename_stem(
#             local_decompressed_file_basename)
#         local_out_json_file = os.path.join(
#             local_decompressed_file_dirname,
#             '.'.join((local_decompressed_filename_stem,
#                       'json')))
#         s3_out_json_file_key = os.path.join(
#             s3_out_files_json_dir_key,
#             ntpath.basename(local_out_json_file))
#         s3_decompressed_file = os.path.join(
#             s3_out_files_decompressed_dir_key,
#             local_decompressed_file_basename
#         )
#
#         if s3_exists(s3_client, bucket_name,
#                      s3_out_json_file_key) and not force_convert:
#             log_msg = '{} -- Skipping deltacon of {} because its JSON '\
#                 'counterpart already exists at {}'.format(
#                     get_timestamp(2),
#                     s3_decompressed_file,
#                     s3_out_json_file_key)
#             with open(local_in_file_deltacon_process_log, 'a+') as lf:
#                 lf.write(log_msg)
#                 lf.write('\n')
#             logger.debug(log_msg)
#             os.remove(local_decompressed_file)
#
#             continue
#
#         if not os.path.exists(local_decompressed_file):
#             s3_client.download_file(
#                 bucket_name,
#                 s3_decompressed_file,
#                 local_decompressed_file
#             )
#
#         if not source_conversion_map[source_name]:
#             s3_client.upload_file(
#                 local_decompressed_file,
#                 bucket_name,
#                 s3_out_json_file_key
#             )
#             with open(local_in_file_deltacon_process_log, 'a+') as lf:
#                 lf.write('{} - Successufully copied {} to {} because no'
#                          ' conversion is required'.format(
#                              get_timestamp(2),
#                              s3_decompressed_file,
#                              s3_out_json_file_key))
#                 lf.write('\n')
#
#             bulk_delete_files([
#                 local_decompressed_file])
#
#             continue
#
#         try:
#             logger.debug(
#                 'Converting {}...'.format(
#                     local_decompressed_file))
#             strategy(
#                 local_decompressed_file,
#                 local_out_json_file)
#         except Exception as e:
#             with open(local_in_file_deltacon_process_log, 'a+') as lf:
#                 lf.write('{} - Failed converting {}'.format(
#                     get_timestamp(2),
#                     s3_decompressed_file))
#                 lf.write('\n')
#             logger.debug(
#                 'Failed converting {} due to\n{}'.format(
#                     s3_decompressed_file, e))
#         else:
#             logger.debug('Success converting {} (locally at {}) to {}'.format(
#                 s3_decompressed_file,
#                 local_decompressed_file,
#                 local_out_json_file))
#
#         if json_modifier_map[source_name]:
#             json_modifier(local_out_json_file)
#
#         s3_client.upload_file(
#             local_out_json_file,
#             bucket_name,
#             s3_out_json_file_key
#         )
#         with open(local_in_file_deltacon_process_log, 'a+') as lf:
#             lf.write('{} - Successufully converted {} and '
#                      'uploaded to {}'.format(
#                          get_timestamp(2),
#                          s3_decompressed_file,
#                          s3_out_json_file_key))
#             lf.write('\n')
#
#         bulk_delete_files([
#             local_decompressed_file,
#             local_out_json_file])
#
#     s3_client.upload_file(
#         local_in_file_deltacon_process_log,
#         bucket_name,
#         s3_in_file_deltacon_process_log
#     )
#     logger.debug('Uploaded deltacon process log to {}'.format(
#         s3_in_file_deltacon_process_log))


# def lambda_convert(
#         source_name,
#         bucket_name,
#         infile_basename,
#         outfile_basename,
#         s3_in_file,
#         s3_out_file):
#     local_out_json_file = os.path.join(temp_dir, outfile_basename)
#     local_in_file = s3_download_file(s3_in_file, bucket_name, temp_dir)
#     logger.debug('Downloaded {} to {}'.format(s3_in_file, local_in_file))
#
#     if not source_conversion_map[source_name]:
#         s3_client.upload_file(
#             local_in_file,
#             bucket_name,
#             s3_out_file
#         )
#         logger.debug('Successfully copied {} to {} because no'
#                      ' conversion is required'.format(
#                          s3_in_file,
#                          s3_out_file))
#         return
#
#     try:
#         logger.debug(
#             'Converting {}...'.format(
#                 local_in_file))
#         strategy(
#             local_in_file,
#             local_out_json_file)
#     except Exception as e:
#         logger.debug(
#             'Failed converting {} due to\n{}'.format(
#                 s3_in_file, e))
#     else:
#         if json_modifier_map[source_name]:
#             json_modifier(local_out_json_file)
#         s3_client.upload_file(
#             local_out_json_file,
#             bucket_name,
#             s3_out_file
#         )
#         logger.debug('Successfully converted {} '
#                      '(locally at {}) to {} and uploaded it '
#                      'to s3_out_file'.format(
#                          s3_in_file,
#                          local_in_file,
#                          local_out_json_file,
#                          s3_out_file))


def json_modifier(json_file):
    with codecs.open(json_file, 'rb') as jf:
        content = rapidjson.load(jf)
        content['id'] = [item for item in content[
            'doc']['str'] if item['name'] == 'id'][0]['text']

    with open(json_file, 'w', encoding='UTF-8') as fo:
        rapidjson.dump(content, fo)


def strategy_runner(
        s3_client,
        strategy_id,
        bucket_name,
        csv_writer,
        source_name,
        process_date,
        comparison_date=None,
        ignore_log=False,
        force_convert=False,
        delta_only=False):

    strategy_map = {
        1: strategy1,
        2: strategy2,
        3: strategy3,
        4: strategy4
    }

    strategy = strategy_map[strategy_id]
    num_succeeded = 0
    num_failed = 0
    num_skipped = 0

    strategy_runner_processing_start = time.time()
    comparison_s3_dir = None

    input_s3_dir = os.path.join(
        'data',
        process_date.strftime('%Y/%m/%d'),
        'structured',
        'raw',
        source_name)

    logger.debug(
        'STARTING STRATEGY #{} for {}...'.format(strategy_id, input_s3_dir))

    if comparison_date:
        comparison_s3_dir = os.path.join(
            'data',
            comparison_date.strftime('%Y/%m/%d'),
            'structured',
            'processed',
            source_name,
            'decompressed')

    resp = s3_client.list_objects(Bucket=bucket_name, Prefix=input_s3_dir)
    contents = resp['Contents']

    if not delta_only:
        (s3_in_file_deltacon_process_log,
         local_in_file_deltacon_process_log,
         s3_out_files_decompressed_dir_key,
         s3_out_files_json_dir_key,
         local_out_decompressed_dir_all
         ) = decompressor(s3_client, input_s3_dir, bucket_name, contents,
                          source_name, ignore_log)
    else:
        (s3_in_file_decompression_process_log,
         local_in_file_decompression_process_log,
         s3_in_file_deltacon_process_log,
         local_in_file_deltacon_process_log,
         s3_out_files_decompressed_dir_key,
         s3_out_files_json_dir_key,
         local_out_decompressed_dir_all) = setup_s3_dir_level_nomenclature(
            input_s3_dir, source_name)

    deltacon(
        s3_client,
        bucket_name,
        strategy,
        source_name,
        s3_in_file_deltacon_process_log,
        local_in_file_deltacon_process_log,
        s3_out_files_decompressed_dir_key,
        s3_out_files_json_dir_key,
        local_out_decompressed_dir_all,
        comparison_s3_dir,
        ignore_log,
        force_convert
    )

    # strategy_runner_processing_end = time.time()
    # strategy_runner_processing_time = strategy_runner_processing_end \
    #     - strategy_runner_processing_start

    # file_transfer_overhead_time = round(strategy_runner_processing_time
    #                                     - total_strategy_time, 2)

    # processing_speed = readable_size(
    #     total_input_file_size / total_strategy_time, True)
    # total_input_file_size = readable_size(total_input_file_size)
    # total_output_file_size = readable_size(total_output_file_size)

    # strategy_runner_processing_time = round(
    #     strategy_runner_processing_time, 2)
    # total_strategy_time = round(total_strategy_time, 2)

    # logger.debug('Strategy #{} Total Succeeded: {}'.format(
    #     strategy_id, num_succeeded))
    # logger.debug('Strategy #{} Total Failed: {}'.format(
    #     strategy_id, num_failed))
    # logger.debug('Strategy #{} Total Conversion time: '
    #              '{} seconds'.format(
    #                  strategy_id, total_strategy_time))
    # logger.debug('Strategy #{} Total File Transfer Overhead '
    #              'time: {} seconds'.format(
    #                  strategy_id, file_transfer_overhead_time))
    # logger.debug('Strategy #{} Total Strategy Running '
    #              'time: {} seconds'.format(
    #                  strategy_id, strategy_runner_processing_time))
    # logger.debug('Strategy #{} Total Input File Size: {}'.format(
    #     strategy_id, total_input_file_size))
    # logger.debug('Strategy #{} Total Output Fize Size: {}'.format(
    #     strategy_id, total_output_file_size))
    # logger.debug('Strategy #{} Total Files processed: {}'.format(
    #     strategy_id, total_files_processed))
    # logger.debug('Strategy #{} Average Processing Speed: {}'.format(
    #     strategy_id, processing_speed))

    # csv_writer.writerow([
    #     'Strategy {}'.format(strategy_id),
    #     total_files_processed,
    #     num_succeeded,
    #     num_failed,
    #     total_input_file_size,
    #     total_output_file_size,
    #     '{} secs'.format(total_strategy_time),
    #     '{} secs'.format(file_transfer_overhead_time),
    #     '{} secs'.format(strategy_runner_processing_time),
    #     processing_speed
    # ])

    return s3_out_files_decompressed_dir_key


def run_strategies(args):
    # logger.debug('Attempting to write metrics to {}'.format(output_csv_file))

    process_date = args.processdate
    source_name = args.source

    if args.datedelta:
        comparison_date = process_date - datetime.timedelta(args.datedelta)
    elif args.comparedate:
        comparison_date = args.comparedate
    else:
        comparison_date = None

    with open(output_csv_file, 'w') as csvfile:
        csv_writer = csv.writer(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        csv_writer.writerow([
            'Strategy',
            'Processed',
            'Succeeded',
            'Failed',
            'Input Size',
            'Output Size',
            'Conversion Time',
            'Transfer Time',
            'Total Time',
            'Conversion Rate'
        ])

        s3_client = get_s3_client(True)
        s3_out_files_decompressed_dir_key = strategy_runner(
            s3_client,
            1, bucket_1,
            csv_writer, args.source, process_date, comparison_date,
            args.ignore_log, args.force_convert, args.delta_only)
        # strategy_runner(2, 'oldsax', csv_writer)
        # strategy_runner(3, 'jxmlease', csv_writer)
        # strategy_runner(4, 'etree', csv_writer)
    os.remove(output_csv_file)


def move_files(
        s3_client,
        contents,
        source_name,
        extension,
        s3_root_dir_key):
    # logger.debug(contents)
    for idx, content in enumerate(contents):
        content_key = content['Key']

        if content_key.endswith(extension) and '{}/'.format(
                source_name) not in content_key:
            s3_in_file_basename = ntpath.basename(content_key)
            s3_in_file_dirname = os.path.dirname(content_key)
            local_in_file = os.path.join(temp_dir, s3_in_file_basename)
            out_key = os.path.join(
                s3_in_file_dirname,
                source_name,
                s3_in_file_basename
            )

            if not s3_exists(s3_client, bucket_1, out_key):
                s3_client.download_file(
                    bucket_1, content_key, local_in_file)

                time.sleep(0.1)

                s3_client.upload_file(
                    local_in_file,
                    bucket_1,
                    out_key)

                time.sleep(0.1)

                if s3_exists(s3_client, bucket_1, out_key):
                    logger.debug('{} moved to {}'.format(content_key, out_key))
                    s3_client.delete_object(
                        Bucket=bucket_1,
                        Key=content_key
                    )
                    os.remove(local_in_file)
                    logger.debug('{} on S3 deleted'.format(content_key))
                    logger.debug('Local file {} deleted'.format(local_in_file))
                else:
                    logger.debug('{} NOT moved to {}'.format(
                        content_key, out_key))


def s3_shuffle():
    s3_root_dir_key = 'data/2018/03/29/structured/processed'
    # s3_root_dir_key = 'data/{}/structured'.format(
    #     datetime.datetime.now().strftime('%Y/%m/%d'))
    # inputs_s3_dir = os.path.join('inputs', 'xml')
    # outputs_s3_dir = os.path.join('outputs', 'xml')
    s3_client = get_s3_client()
    # iresp = s3_client.list_objects(Bucket=bucket_1, Prefix=inputs_s3_dir)
    oresp = s3_client.list_objects(Bucket=bucket_1, Prefix=s3_root_dir_key)
    # oresp = s3_client.list_objects(Bucket=bucket_1, Delimiter='/')

    # logger.debug(oresp)

    move_targets = [
        # (iresp['Contents'], '.xml', 'raw'),
        (oresp['Contents'], 'pubmed', '.json')
    ]

    for move_target in move_targets:
        move_files(
            s3_client,
            move_target[0],
            move_target[1],
            move_target[2],
            s3_root_dir_key)


def get_iterable_list():
    s3_client = get_s3_client()
    bucket_name = bucket_1
    prefix = 'data/2018/03/29/structured/processed'
    jprefix = 'data/2018/04/10/structured/processed/nct/decompressed'
    page_limit = 1000
    pages = None

    with open('pubmed_processed_xml_04', 'w') as xf:
        list_dir(
            s3_client,
            bucket_name,
            prefix,
            page_limit,
            pages,
            '',
            xf,
            # True
        )

    # with open('nct_decompressed_10', 'w') as jf:
    #     list_dir(
    #         s3_client,
    #         bucket_name,
    #         jprefix,
    #         page_limit,
    #         pages,
    #         '',
    #         jf,
    #         True
    #     )


def bulk_download_s3(
        local_root,
        s3_client,
        bucket_name,
        prefix,
        page_limit,
        pages,
        delimiter):
    local_file = os.path.join(local_root, 'data.txt')
    # local_dir = os.path.join(local_root, prefix)
    # if not os.path.exists(local_dir):
    #     os.makedirs(local_dir)

    with open(local_file, 'w') as file_writer:
        list_dir(
            s3_client,
            bucket_name,
            prefix,
            page_limit,
            pages,
            delimiter,
            file_writer
        )

    # for s3_key in key_list:
    #     local_file = os.path.join(local_root, s3_key)
    #     s3_client.download_file(
    #         bucket_name,
    #         s3_key,
    #         local_file)


def bulk_download_pubmed():
    root_dir = os.path.join(os.path.expanduser('~'), 'medmeme')
    s3_client = get_s3_client()
    folder_prefix = 'data/2018/04/09/structured/processed/dailymed/json/'

    bulk_download_s3(
        root_dir,
        s3_client,
        bucket_1,
        folder_prefix,
        100,
        None,
        ''
    )


def delta_calculator(file1, file2, file3):
    with open(file1) as f1:
        f1_lines = f1.readlines()

    with open(file2) as f2:
        f2_lines = f2.readlines()

    diff = difflib.unified_diff(
        f1_lines, f2_lines, n=0)

    with open(file3, 'w') as f3:
        f3.writelines(diff)

    # os.remove(file1)
    # os.remove(file2)

    return file1, file2, file3, diff


def calculate_delta(s3_client, bucket_name, s3_folder_1, s3_folder_2):
    page_limit = 100

    file1 = os.path.join(temp_dir, 's3list1.txt')
    file2 = os.path.join(temp_dir, 's3list2.txt')
    file3 = os.path.join(temp_dir, 'out.txt')

    with open(file1, 'w') as file_writer:
        list_dir(
            s3_client,
            bucket_name,
            s3_folder_1,
            page_limit,
            None,
            '',
            file_writer
        )

    with open(file2, 'w') as file_writer:
        list_dir(
            s3_client,
            bucket_name,
            s3_folder_2,
            page_limit,
            None,
            '',
            file_writer
        )

    return delta_calculator(file1, file2, file3)


def delta_caller():
    s3_folder_1 = 'data/2018/04/09/structured/processed/nct/decompressed'
    s3_folder_2 = 'data/2018/04/10/structured/processed/nct/decompressed'
    s3_client = get_s3_client()
    return calculate_delta(
        s3_client, bucket_1, s3_folder_1, s3_folder_2)


def get_args():
    parser = ArgumentParser()
    parser.add_argument('-s', '--source', nargs='?', help='medical source')
    parser.add_argument('-p', '--processdate', nargs='?',
                        type=lambda p: datetime.datetime.strptime(
                            p, '%Y-%m-%d'),
                        help='processing date -- format YYYY-MM-DD')
    parser.add_argument('-d', '--datedelta', nargs='?',
                        type=int,
                        help='date delta (in number of days)')
    parser.add_argument('-c', '--comparedate', nargs='?',
                        type=lambda d: datetime.datetime.strptime(
                            d, '%Y-%m-%d'),
                        help='comparison date -- format YYYY-MM-DD')
    parser.add_argument('--ignore-log', dest='ignore_log',
                        action='store_true')
    parser.add_argument('--force-convert', dest='force_convert',
                        action='store_true')
    parser.add_argument('--delta-only', dest='delta_only',
                        action='store_true')

    return parser.parse_args()


def main():
    # download_from_pubmed_to_s3()
    args = get_args()
    run_strategies(args)
    # s3_shuffle()
    shutil.rmtree(temp_dir)


if __name__ == '__main__':
    main()

