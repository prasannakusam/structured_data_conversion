import xml.etree.cElementTree as ElementTree
from boto3.session import Session
from saxp36 import XmlToJson
from ftplib import (
    FTP,
    error_temp,
    error_perm
)
import rapidjson
import xmltodict
import jxmlease
import datetime
import tempfile
import logging
import codecs
import shutil
import ntpath
import time
import gzip
import sys
import csv
import os


root_dir = os.path.join(os.path.expanduser('~'), 'data')
output_log_dir = os.path.join(os.path.expanduser('~'), 'logs')
output_log_file = os.path.join(output_log_dir, 'output.{}.log'.format(
    datetime.datetime.now().strftime('%Y%m%d%H%M%S')))
output_csv_file = os.path.join(output_log_dir, 'metrics.{}.csv'.format(
    datetime.datetime.now().strftime('%Y%m%d%H%M%S')))
bucket_name = 'xconverter'
temp_dir = tempfile.mkdtemp()
strategy_runner_times = dict()


class Error(Exception):
    pass


class KeyNotFoundError(Error):
    def __init__(self, msg):
        self.message = msg


def get_s3_client():
    session = Session(
        aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'),
        aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY')
    )

    return session.client('s3')


def get_logger(filename, log_file=None):
    logger = logging.getLogger(filename)
    logger.setLevel(logging.DEBUG)

    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    channel = logging.FileHandler(log_file) if log_file else \
        logging.StreamHandler(sys.stdout)
    channel.setLevel(logging.DEBUG)
    channel.setFormatter(formatter)

    logger.addHandler(channel)

    return logger


logger = get_logger(__file__)


def escape(ipstr):
    ipstr = ipstr.replace("&", "&amp;")
    ipstr = ipstr.replace("p<0.05", "p=0.05")
    ipstr = ipstr.replace("p<0,", "p=0.")
    ipstr = ipstr.replace("p < 0,", "p=0.")
    ipstr = ipstr.replace("p <0,", "p=0.")

    return str


def timefunc(f):
    def f_timer(*args, **kwargs):
        start = time.time()
        result = f(*args, **kwargs)
        end = time.time()
        time_taken = end - start
        strategy_runner_times[args[0]] = time_taken
        logger.debug('Strategy Runner for '
                     'Strategy#{} took: {} seconds'.format(
                         args[0], time_taken))

        return result
    return f_timer


def size_formatter(value, unit, is_speed=False):
    out_unit = unit if not is_speed else '{}/s'.format(unit)

    return '{} {}'.format(value, out_unit)


def readable(value, is_speed=False):
    kilos = 1024
    megs = kilos * 1024
    gigs = megs * 1024
    teras = gigs * 1024

    if value >= teras:
        return size_formatter(round(value/teras, 2), 'TB', is_speed)
    elif value < teras and value >= gigs:
        return size_formatter(round(value/gigs, 2), 'GB', is_speed)
    elif value < gigs and value >= megs:
        return size_formatter(round(value/megs, 2), 'MB', is_speed)
    elif value < megs and value >= kilos:
        return size_formatter(round(value/kilos, 2), 'KB', is_speed)
    else:
        return size_formatter(value, 'bytes', is_speed)


def xml_element_to_dict(elem):
    """
    Convert XML Element to a simple dict

    """
    inner = dict(elem.attrib)
    children = list(map(xml_element_to_dict, elem.getchildren()))
    text = elem.text and elem.text.strip()
    if text:
        inner['@text'] = text
    if children:
        inner['@children'] = children

    return {elem.tag: inner}


def s3_exists(client, bucket, key):
    resp = client.list_objects(
        Bucket=bucket,
        Prefix=key)
    expected_object = resp.get('Contents', [])

    return bool(expected_object)


def setup_nomenclature(key, strategy_dir):
    s3_in_file = key
    s3_in_file_dir_key = os.path.dirname(s3_in_file)
    s3_in_file_basename = ntpath.basename(s3_in_file)
    s3_in_filename, s3_in_file_extension = os.path.splitext(
        s3_in_file_basename)
    local_in_file = os.path.join(
        temp_dir, ntpath.basename(s3_in_file_basename))
    local_out_filename, local_out_file_extension = s3_in_filename, '.json'
    local_out_file_basename = ''.join(
        (local_out_filename, local_out_file_extension))
    local_out_file = os.path.join(temp_dir, local_out_file_basename)
    s3_out_file_dir_key = s3_in_file_dir_key.replace('inputs', 'outputs')
    s3_out_file_dir_key_list = s3_out_file_dir_key.split(os.sep)
    s3_out_file_dir_key_xml_index = s3_out_file_dir_key_list.index('xml')
    s3_out_file_dir_key_list.insert(
        s3_out_file_dir_key_xml_index + 1, strategy_dir)
    s3_out_file_dir_key = os.sep.join(s3_out_file_dir_key_list)
    s3_out_file = os.path.join(s3_out_file_dir_key, local_out_file_basename)

    return (s3_in_file,
            local_in_file,
            s3_out_file,
            local_out_file)


def download_from_pubmed_to_s3():
    input_xml_dir = os.path.join(root_dir, 'inputs', 'xml')
    s3_client = get_s3_client()

    ftp_root = 'ftp.ncbi.nlm.nih.gov'
    ftp_paths = [
        '/pubmed/baseline',
        '/pubmed/updatefiles',
    ]

    for ftp_path in ftp_paths:
        local_ftp_dir = os.path.join(input_xml_dir, *ftp_path.split(os.sep))
        local_ftp_dir_list = local_ftp_dir.split(os.sep)
        s3_dir_key = os.path.join(*local_ftp_dir_list[
            local_ftp_dir_list.index('inputs'):])

        if not os.path.exists(local_ftp_dir):
            os.makedirs(local_ftp_dir)

        ftp_client = FTP(ftp_root)
        ftp_client.login()
        ftp_client.cwd(ftp_path)

        for idx, gfile in enumerate(ftp_client.nlst()):
            if gfile.endswith('.xml.gz'):
                xfile = gfile.replace('.gz', '')
                local_ftp_gzip_file = os.path.join(local_ftp_dir, gfile)
                local_ftp_xml_file = os.path.join(
                    local_ftp_dir, xfile)
                s3_file_key = os.path.join(s3_dir_key, xfile)

                if not s3_exists(s3_client, bucket_name, s3_file_key):
                    upload_to_s3 = True

                    if not os.path.exists(local_ftp_xml_file):
                        with open(local_ftp_gzip_file, 'wb') as lf:
                            try:
                                ftp_client.retrbinary(
                                    'RETR {}'.format(gfile), lf.write)
                            except (error_temp, error_perm):
                                upload_to_s3 = False
                                logger.debug('Unable to download'
                                             ' {}. Skipping...'.format(
                                                 gfile))
                            else:
                                with gzip.open(local_ftp_gzip_file,
                                               'rb') as gf:
                                    with open(local_ftp_xml_file, 'wb') as xf:
                                        shutil.copyfileobj(gf, xf)
                                os.remove(local_ftp_gzip_file)

                    if upload_to_s3:
                        s3_client.upload_file(
                            local_ftp_xml_file,
                            bucket_name,
                            s3_file_key)
                        os.remove(local_ftp_xml_file)

                        logger.debug('Moved {} to S3'.format(
                            local_ftp_xml_file))
                else:
                    logger.debug('{} is already available in S3'.format(
                        local_ftp_xml_file))


def strategy1(input_file, output_file):
    """
    `xmltodict` library based strategy

    """
    with open(output_file, 'w', encoding='UTF-8') as fo:
        with codecs.open(input_file, 'rb') as fi:
            rapidjson.dump(xmltodict.parse(fi), fo, indent=4)


def strategy2(input_file, output_file):
    """
    sax based strategy (old-style)

    """
    p = XmlToJson(input=input_file, output=output_file, indent=4)
    p.parse()


def strategy3(input_file, output_file):
    """
    `jxmlease` library based strategy

    """
    with open(output_file, 'w') as fo:
        with codecs.open(input_file, 'rb') as fi:
            rapidjson.dump(jxmlease.parse(fi), fo, indent=4)


def strategy4(input_file, output_file):
    """
    `etree` based strategy

    """
    xml_parser = ElementTree.parse(input_file)

    root_dir = xml_parser.getroot()

    with open(output_file, 'w') as fo:
        rapidjson.dump(xml_element_to_dict(root_dir), fo, indent=4)


def strategy_runner(strategy_id, strategy_dir, csv_writer):
    strategy_map = {
        1: strategy1,
        2: strategy2,
        3: strategy3,
        4: strategy4
    }

    total_strategy_time = 0
    num_succeeded = 0
    num_failed = 0
    total_input_file_size = 0
    total_output_file_size = 0

    logger.debug(
        'STARTING STRATEGY #{}...'.format(strategy_id))

    strategy_runner_processing_start = time.time()

    input_s3_dir = os.path.join('inputs', 'xml', 'pubmed')
    s3_client = get_s3_client()
    resp = s3_client.list_objects(Bucket=bucket_name, Prefix=input_s3_dir)

    for content in resp['Contents']:
        content_key = content['Key']

        if content_key.endswith('.xml'):
            (s3_in_file,
             local_in_file,
             s3_out_file,
             local_out_file) = setup_nomenclature(content_key, strategy_dir)

            if not s3_exists(s3_client, bucket_name, s3_out_file):
                upload_to_s3 = True
                remove_local_out_file = True
                s3_client.download_file(bucket_name, s3_in_file, local_in_file)

                strategy = strategy_map[strategy_id]

                processing_start = time.time()
                try:
                    logger.debug(
                        'Processing {}...'.format(local_in_file))
                    processing_time = strategy(local_in_file, local_out_file)
                except Exception as e:
                    upload_to_s3 = False
                    remove_local_out_file = False
                    num_failed += 1
                    logger.debug(
                        'Failed processing {} due to\n{}'.format(
                            local_in_file, e))
                else:
                    upload_to_s3 = True
                    num_succeeded += 1
                    current_input_file_size = os.path.getsize(local_in_file)
                    current_output_file_size = os.path.getsize(local_out_file)
                    total_input_file_size += current_input_file_size
                    total_output_file_size += current_output_file_size
                    logger.debug(
                        'Success processing {}'.format(
                            local_in_file))
                processing_end = time.time()

                total_file_processing_time = processing_end - processing_start
                total_strategy_time += total_file_processing_time
                total_files_processed = num_succeeded + num_failed

                logger.debug('Conversion time (running total): {}'.format(
                    total_strategy_time))
                logger.debug('Files processed (running total): {}'.format(
                    total_files_processed))
                logger.debug('Files Succeeded (running total): {}'.format(
                    num_succeeded))
                logger.debug('Files Failed (running total): {}'.format(
                    num_failed))
                logger.debug('Input File Size (running total): {}'.format(
                    readable(total_input_file_size)))
                logger.debug('Output Fize Size (running total): {}'.format(
                    readable(total_output_file_size)))
                logger.debug('Conversion Rate (running total): {}'.format(
                    readable(
                        total_input_file_size / total_strategy_time, True)))

                if upload_to_s3:
                    s3_client.upload_file(
                        local_out_file,
                        bucket_name,
                        s3_out_file)
                os.remove(local_in_file)
                if remove_local_out_file:
                    os.remove(local_out_file)

            else:
                logger.debug('Skipping {} because it already'
                             ' exists in S3'.format(s3_out_file))

        else:
            logger.debug('Skipping {} because it is not an'
                         ' XML file'.format(content_key))

    strategy_runner_processing_end = time.time()
    strategy_runner_processing_time = strategy_runner_processing_end \
        - strategy_runner_processing_start

    file_transfer_overhead_time = round(strategy_runner_processing_time
                                        - total_strategy_time, 2)

    processing_speed = readable(
        total_input_file_size / total_strategy_time, True)
    total_input_file_size = readable(total_input_file_size)
    total_output_file_size = readable(total_output_file_size)

    strategy_runner_processing_time = round(
        strategy_runner_processing_time, 2)
    total_strategy_time = round(
        total_strategy_time, 2)

    logger.debug('Strategy #{} Total Succeeded: {}'.format(
        strategy_id, num_succeeded))
    logger.debug('Strategy #{} Total Failed: {}'.format(
        strategy_id, num_failed))
    logger.debug('Strategy #{} Total Conversion time: '
                 '{} seconds'.format(
                     strategy_id, total_strategy_time))
    logger.debug('Strategy #{} Total File Transfer Overhead '
                 'time: {} seconds'.format(
                     strategy_id, file_transfer_overhead_time))
    logger.debug('Strategy #{} Total Strategy Running '
                 'time: {} seconds'.format(
                     strategy_id, strategy_runner_processing_time))
    logger.debug('Strategy #{} Total Input File Size: {}'.format(
        strategy_id, total_input_file_size))
    logger.debug('Strategy #{} Total Output Fize Size: {}'.format(
        strategy_id, total_output_file_size))
    logger.debug('Strategy #{} Total Files processed: {}'.format(
        strategy_id, total_files_processed))
    logger.debug('Strategy #{} Average Processing Speed: {}'.format(
        strategy_id, processing_speed))

    csv_writer.writerow([
        'Strategy {}'.format(strategy_id),
        total_files_processed,
        num_succeeded,
        num_failed,
        total_input_file_size,
        total_output_file_size,
        '{} secs'.format(total_strategy_time),
        '{} secs'.format(file_transfer_overhead_time),
        '{} secs'.format(strategy_runner_processing_time),
        processing_speed
    ])


def run_strategies():
    logger.debug('Attempting to write metrics to {}'.format(output_csv_file))
    with open(output_csv_file, 'w') as csvfile:
        csv_writer = csv.writer(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        csv_writer.writerow([
            'Strategy',
            'Processed',
            'Succeeded',
            'Failed',
            'Input Size',
            'Output Size',
            'Conversion Time',
            'Transfer Time',
            'Total Time',
            'Conversion Rate'
        ])
        # strategy_runner(1, 'xmltodict', csv_writer)
        # strategy_runner(2, 'oldsax', csv_writer)
        # strategy_runner(3, 'jxmlease', csv_writer)
        strategy_runner(4, 'etree', csv_writer)


def move_files(
        s3_client,
        contents,
        extension,
        folder_name,
        s3_root_dir_key):

    for idx, content in enumerate(contents):
        content_key = content['Key']

        if content_key.endswith(extension):
            s3_in_file_basename = ntpath.basename(content_key)
            local_in_file = os.path.join(temp_dir, s3_in_file_basename)
            content_key_list = content_key.split(os.sep)
            content_key_list_xml_index = content_key_list.index('xml')
            out_key = os.path.join(
                s3_root_dir_key,
                folder_name,
                *content_key_list[content_key_list_xml_index + 1:])

            if not s3_exists(s3_client, bucket_name, out_key):
                s3_client.download_file(
                    bucket_name, content_key, local_in_file)

                time.sleep(0.1)

                s3_client.upload_file(
                    local_in_file,
                    bucket_name,
                    out_key)

                time.sleep(0.1)

                if s3_exists(s3_client, bucket_name, out_key):
                    logger.debug('{} moved to {}'.format(content_key, out_key))
                    s3_client.delete_object(
                        Bucket=bucket_name,
                        Key=content_key
                    )
                    os.remove(local_in_file)
                    logger.debug('{} on S3 deleted'.format(content_key))
                    logger.debug('Local file {} deleted'.format(local_in_file))
                else:
                    logger.debug('{} NOT moved to {}'.format(
                        content_key, out_key))


def s3_shuffle():
    s3_root_dir_key = 'data/2018/03/28/structured'
    # s3_root_dir_key = 'data/{}/structured'.format(
    #     datetime.datetime.now().strftime('%Y/%m/%d'))
    inputs_s3_dir = os.path.join('inputs', 'xml')
    outputs_s3_dir = os.path.join('outputs', 'xml')
    s3_client = get_s3_client()
    iresp = s3_client.list_objects(Bucket=bucket_name, Prefix=inputs_s3_dir)
    oresp = s3_client.list_objects(Bucket=bucket_name, Prefix=outputs_s3_dir)

    move_targets = (
        (oresp['Contents'], '.json', 'processed')
    )

    for move_target in move_targets:
        move_files(
            s3_client,
            move_target[0],
            move_target[1],
            move_target[2],
            s3_root_dir_key)


def main():
    # download_from_pubmed_to_s3()
    # run_strategies()
    s3_shuffle()
    shutil.rmtree(temp_dir)


if __name__ == '__main__':
    main()
